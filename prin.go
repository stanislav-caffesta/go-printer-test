package main

import (
	"github.com/alexbrainman/printer"
	"fmt"
	"os"
)

func main() {
	name, err := printer.Default()
	if err != nil {
		fmt.Printf("Default failed: %v", err)
	}

	p, err := printer.Open(name)
	if err != nil {
		fmt.Printf("Open failed: %v", err)
	}
	defer p.Close()

	err = p.StartDocument("caffesta", "RAW")
	f, err := os.Open("1.txt")
	if err != nil {
		fmt.Printf("Open failed: %v", err)
	}
	defer p.EndDocument()

	b1 := make([]byte, 1000)
	n1, err := f.Read(b1)
	s := string(b1[:n1])
	if err != nil {
		fmt.Printf("Open failed: %v", err)
	}

	err = p.StartPage()
	if err != nil {
		fmt.Printf("StartPage failed: %v", err)
	}
	fmt.Fprintf(p, s, name)
	err = p.EndPage()
	if err != nil {
		fmt.Printf("EndPage failed: %v", err)
	}
}